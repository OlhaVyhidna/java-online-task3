package com.mycompany.app.exceptions.autocloseable;

import com.mycompany.app.exceptions.myexceptions.MyFirstException;

public class MyAutoCloseable implements AutoCloseable {


    public void workWithResources(){
        System.out.println("Working with resources");
    }

    public void close() throws Exception {
        System.out.println("MyAutoCloseable. Close resource");
        throw new MyFirstException("My first exception");
    }
}
