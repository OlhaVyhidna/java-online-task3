package com.mycompany.app.exceptions.myexceptions;

public class MyFirstException extends Exception {
    public MyFirstException() {
    }

    public MyFirstException(String message) {
        super(message);
    }

    public MyFirstException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyFirstException(Throwable cause) {
        super(cause);
    }
}
