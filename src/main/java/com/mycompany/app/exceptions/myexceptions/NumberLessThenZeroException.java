package com.mycompany.app.exceptions.myexceptions;

public class NumberLessThenZeroException extends Exception {
    public NumberLessThenZeroException() {
    }

    public NumberLessThenZeroException(String message) {
        super(message);
    }

    public NumberLessThenZeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public NumberLessThenZeroException(Throwable cause) {
        super(cause);
    }
}
