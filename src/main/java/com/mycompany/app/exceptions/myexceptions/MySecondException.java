package com.mycompany.app.exceptions.myexceptions;

public class MySecondException extends Exception {
    public MySecondException() {
    }

    public MySecondException(String message) {
        super(message);
    }

    public MySecondException(String message, Throwable cause) {
        super(message, cause);
    }

    public MySecondException(Throwable cause) {
        super(cause);
    }
}
