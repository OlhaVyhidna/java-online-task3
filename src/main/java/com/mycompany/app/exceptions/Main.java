package com.mycompany.app.exceptions;

import com.mycompany.app.exceptions.autocloseable.MyAutoCloseable;

public class Main {

    public static void main(String[] args) {

        try (MyAutoCloseable myAutoCloseable = new MyAutoCloseable()){
            myAutoCloseable.workWithResources();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
