package com.mycompany.app.ooptask;

import com.mycompany.app.ooptask.menu.Menu;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {


        try (Scanner scanner = new Scanner(System.in)) {
            final Menu menu = new Menu();
            menu.startUpMenu(scanner);
        } catch (Exception e) {
            System.out.println(e.getClass().getCanonicalName() + " " + e.getMessage());
        }

    }
}
