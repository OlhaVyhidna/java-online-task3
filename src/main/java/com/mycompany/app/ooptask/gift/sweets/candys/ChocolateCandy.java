package com.mycompany.app.ooptask.gift.sweets.candys;

import java.util.Objects;

public class ChocolateCandy extends Candy{

    private int weightInGrams;
    private  int gramsOfSugarInProduct;
    private final int sugarRatioInPercents = 64;

    public ChocolateCandy(int weightInGrams) {
        this.weightInGrams = weightInGrams;
        this.gramsOfSugarInProduct = findGramsOfSugarInProduct();
    }

    @Override
    void shapeACandy() {
        System.out.println("forms rectangular candy");
    }


    @Override
    public int getGramsOfSugarInProduct() {
        return gramsOfSugarInProduct;
    }

    public void setGramsOfSugarInProduct(int gramsOfSugarInProduct) {
        this.gramsOfSugarInProduct = gramsOfSugarInProduct;
    }

    public int getSugarRatioInPercents() {
        return sugarRatioInPercents;
    }

    public int getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightiNGrams(int weightiNGrams) {
        this.weightInGrams = weightiNGrams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChocolateCandy that = (ChocolateCandy) o;
        return weightInGrams == that.weightInGrams &&
                gramsOfSugarInProduct == that.gramsOfSugarInProduct &&
                sugarRatioInPercents == that.sugarRatioInPercents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightInGrams, gramsOfSugarInProduct, sugarRatioInPercents);
    }

    @Override
    public String toString() {
        return "ChocolateCandy{" +
                "weightInGrams=" + weightInGrams +
                ", gramsOfSugarInProduct=" + gramsOfSugarInProduct +
                ", sugarRatioInPercents=" + sugarRatioInPercents +
                '}';
    }
}
