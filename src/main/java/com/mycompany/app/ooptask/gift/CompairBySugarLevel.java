package com.mycompany.app.ooptask.gift;

import com.mycompany.app.ooptask.gift.sweets.Sweet;

import java.util.Comparator;

public class CompairBySugarLevel implements Comparator<Sweet> {


    @Override
    public int compare(Sweet o1, Sweet o2) {
        return o1.getGramsOfSugarInProduct() - o2.getGramsOfSugarInProduct();
    }
}
