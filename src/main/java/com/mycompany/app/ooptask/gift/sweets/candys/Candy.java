package com.mycompany.app.ooptask.gift.sweets.candys;

import com.mycompany.app.ooptask.gift.sweets.Sweet;

public abstract class Candy implements Sweet {


    @Override
    public void packTheSweet() {
        System.out.println("packaging in candy paper");
    }

    abstract void shapeACandy();
    abstract int getSugarRatioInPercents();

    public int findGramsOfSugarInProduct() {
        return (getWeightInGrams()*(getSugarRatioInPercents()))/100;
    }
}
