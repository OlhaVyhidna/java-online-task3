package com.mycompany.app.ooptask.gift.sweets;

public interface Sweet {

    int getWeightInGrams();

    int getGramsOfSugarInProduct();

    void packTheSweet();
}
