package com.mycompany.app.ooptask.gift.sweets;

import java.util.Objects;

public class ChocolateSantaClaus implements Sweet {


    private int weightGrams;
    private int gramsOfSugarInProduct;
    private final int sugarRatioInPercents = 64;

    public ChocolateSantaClaus(int weightGrams) {
        this.weightGrams = weightGrams;
        this.gramsOfSugarInProduct = findGramsOfSugarInProduct();
    }

    public void makeAChocolateShape() {
        System.out.println("Making a chocolate shape of Santa Claus");
    }

    @Override
    public void packTheSweet() {
        System.out.println("packaging in foil");
    }

    public int findGramsOfSugarInProduct() {
        return getSugarRatioInPercents() * (getWeightInGrams() / 100);
    }

    public int getGramsOfSugarInProduct() {
        return gramsOfSugarInProduct;
    }

    public void setGramsOfSugarInProduct(int gramsOfSugarInProduct) {
        this.gramsOfSugarInProduct = gramsOfSugarInProduct;
    }

    public int getSugarRatioInPercents() {
        return sugarRatioInPercents;
    }

    public int getWeightInGrams() {
        return weightGrams;
    }

    public void setWeightGrams(int weightGrams) {
        this.weightGrams = weightGrams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChocolateSantaClaus that = (ChocolateSantaClaus) o;
        return weightGrams == that.weightGrams &&
                gramsOfSugarInProduct == that.gramsOfSugarInProduct &&
                sugarRatioInPercents == that.sugarRatioInPercents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightGrams, gramsOfSugarInProduct, sugarRatioInPercents);
    }

    @Override
    public String toString() {
        return "ChocolateSantaClaus{" +
                "weightGrams=" + weightGrams +
                ", gramsOfSugarInProduct=" + gramsOfSugarInProduct +
                ", sugarRatioInPercents=" + sugarRatioInPercents +
                '}';
    }
}
