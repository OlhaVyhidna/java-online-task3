package com.mycompany.app.ooptask.gift.sweets.candys;

import java.util.Objects;

public class JellyCandy extends Candy {

    private int weightInGrams;
    private int gramsOfSugarInProduct;
    private final int sugarRatioInPercents = 48;

    public JellyCandy(int weightInGrams) {
        this.weightInGrams = weightInGrams;
        this.gramsOfSugarInProduct = findGramsOfSugarInProduct();
    }

    @Override
    void shapeACandy() {
        System.out.println("shapes candy as a teddy bear");
    }

    @Override
    public int getGramsOfSugarInProduct() {
        return gramsOfSugarInProduct;
    }

    public void setGramsOfSugarInProduct(int gramsOfSugarInProduct) {
        this.gramsOfSugarInProduct = gramsOfSugarInProduct;
    }

    public int getSugarRatioInPercents() {
        return sugarRatioInPercents;
    }

    public int getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(int weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JellyCandy that = (JellyCandy) o;
        return weightInGrams == that.weightInGrams &&
                gramsOfSugarInProduct == that.gramsOfSugarInProduct &&
                sugarRatioInPercents == that.sugarRatioInPercents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightInGrams, gramsOfSugarInProduct, sugarRatioInPercents);
    }

    @Override
    public String toString() {
        return "JellyCandy{" +
                "weightInGrams=" + weightInGrams +
                ", gramsOfSugarInProduct=" + gramsOfSugarInProduct +
                ", sugarRatioInPercents=" + sugarRatioInPercents +
                '}';
    }
}
