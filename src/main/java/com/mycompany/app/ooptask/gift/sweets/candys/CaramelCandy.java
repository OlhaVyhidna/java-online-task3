package com.mycompany.app.ooptask.gift.sweets.candys;

import java.util.Objects;

public class CaramelCandy extends Candy {

    private  int weightInGrams;
    private  int gramsOfSugarInProduct;
    private final int sugarRatioInPercents = 72;

    public CaramelCandy(int weightGrams) {
        this.weightInGrams = weightGrams;
        this.gramsOfSugarInProduct = findGramsOfSugarInProduct();
    }

    @Override
    void shapeACandy() {
        System.out.println("forms round candy");
    }

    @Override
    public int getGramsOfSugarInProduct() {
        return gramsOfSugarInProduct;
    }

    public void setGramsOfSugarInProduct(int gramsOfSugarInProduct) {
        this.gramsOfSugarInProduct = gramsOfSugarInProduct;
    }

    public int getWeightInGrams() {
        return weightInGrams;
    }

    public void setWeightInGrams(int weightInGrams) {
        this.weightInGrams = weightInGrams;
    }



    public int getSugarRatioInPercents() {
        return sugarRatioInPercents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaramelCandy that = (CaramelCandy) o;
        return weightInGrams == that.weightInGrams &&
                gramsOfSugarInProduct == that.gramsOfSugarInProduct &&
                sugarRatioInPercents == that.sugarRatioInPercents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(weightInGrams, gramsOfSugarInProduct, sugarRatioInPercents);
    }

    @Override
    public String
    toString() {
        return "CaramelCandy{" +
                "weightInGrams=" + weightInGrams +
                ", gramsOfSugarInProduct=" + gramsOfSugarInProduct +
                ", sugarRatioInPercents=" + sugarRatioInPercents +
                '}';
    }
}
