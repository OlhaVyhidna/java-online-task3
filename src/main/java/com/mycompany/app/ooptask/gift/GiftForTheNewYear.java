package com.mycompany.app.ooptask.gift;

import com.mycompany.app.ooptask.gift.sweets.Sweet;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class GiftForTheNewYear {

    private ArrayList<Sweet> sweets = new ArrayList<>();


    public ArrayList<Sweet> getSweets() {
        return sweets;
    }

    public void setSweets(ArrayList<Sweet> sweets) {
        this.sweets = sweets;
    }

    public int findWeightOfGift(){
        return (int) getSweets().stream().mapToInt(o -> o.getWeightInGrams()).sum();
    }

    public ArrayList<Sweet> sortSweetsBySugarLevel( int moreThen, int lessThen){
        return (ArrayList<Sweet>) getSweets().stream().filter(sweet -> sweet.getGramsOfSugarInProduct() < lessThen &&
                sweet.getGramsOfSugarInProduct() > moreThen).sorted(new CompairBySugarLevel())
                .collect(Collectors.toList());
    }
}
