package com.mycompany.app.ooptask.menu;

import com.mycompany.app.exceptions.myexceptions.NumberLessThenZeroException;
import com.mycompany.app.ooptask.gift.GiftForTheNewYear;
import com.mycompany.app.ooptask.gift.sweets.ChocolateSantaClaus;
import com.mycompany.app.ooptask.gift.sweets.Sweet;
import com.mycompany.app.ooptask.gift.sweets.candys.CaramelCandy;
import com.mycompany.app.ooptask.gift.sweets.candys.ChocolateCandy;
import com.mycompany.app.ooptask.gift.sweets.candys.JellyCandy;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu {

    private final String CHOOSE_AN_OPTION = "Please, choose an option";
    private final String ADD_SWEETS = "Add sweets in the gift";
    private final String FIND_WEIGHT = "Find weight of the gift";
    private final String FIND_BY_SUGAR = "Find sweets by sugar level";
    private final String DO_YOU_WANT_TO_CONTINUE_FIRST_PART = "If you want to continue ";
    private final String DO_YOU_WANT_TO_CONTINUE_SECOND_PART = " pres 1, if you don't press zero";
    private final String ADD_SWEETS_METHOD_MARKER = "to add sweets as a gift";
    private final String MAIN_MENU_METHOD_MARKER = "work with main menu";


    private GiftForTheNewYear gift;
    private ArrayList<Sweet> sweetsList;


    public Menu() {
        this.gift = new GiftForTheNewYear();
        this.sweetsList = this.gift.getSweets();
    }

    public void startUpMenu(Scanner scanner) throws NumberLessThenZeroException {
        addSweetsToGift(scanner);
        mainMenu(scanner);

    }

    public void mainMenu(Scanner scanner) throws NumberLessThenZeroException {
        System.out.println(CHOOSE_AN_OPTION);
        System.out.println(1 + " - " + ADD_SWEETS);
        System.out.println(2 + " - " + FIND_WEIGHT);
        System.out.println(3 + " - " + FIND_BY_SUGAR);

        int decision = scanner.nextInt();

        switch (decision) {
            case 1: {
                addSweetsToGift(scanner);
                break;
            }
            case 2: {
                findWeight();
                break;
            }
            case 3: {
                findSweetsBySugar(scanner);
                break;
            }
        }

        if (toContinue(scanner, MAIN_MENU_METHOD_MARKER)) {
            mainMenu(scanner);
        }
    }

    public void addSweetsToGift(Scanner scanner) throws NumberLessThenZeroException {
        System.out.println("Please, choose the kind of sweets you want to add in your gift");
        System.out.println(1 + " - " + KindsOFSweets.SANTA.getName());
        System.out.println(2 + " - " + KindsOFSweets.CARAMEL.getName());
        System.out.println(3 + " - " + KindsOFSweets.JELLY.getName());
        System.out.println(4 + " - " + KindsOFSweets.CHOCOLATE.getName());
        int kindOfSweets = scanner.nextInt();
        System.out.println("Enter weight of sweet in grams");
        int weight = scanner.nextInt();
        final Sweet sweet = createSweet(kindOfSweets, weight, scanner);
        this.sweetsList.add(sweet);

        if (toContinue(scanner, ADD_SWEETS_METHOD_MARKER)) {
            addSweetsToGift(scanner);
        }
    }

    public void findWeight() {
        System.out.println(this.gift.findWeightOfGift() + " g - weight of the gift");
    }

    public void findSweetsBySugar(Scanner scanner) {
        System.out.println("Sweet shouldn't contains less sugar then(grams)");
        int moreThen = scanner.nextInt();
        System.out.println("Sweet shouldn't contains more sugar then(grams)");
        int lessThen = scanner.nextInt();

        ArrayList<Sweet> sweets = this.gift.sortSweetsBySugarLevel(moreThen, lessThen);
        System.out.println(sweets);
    }

    public Sweet createSweet(int numberOfDecision, int weight, Scanner scanner) throws NumberLessThenZeroException {
        switch (numberOfDecision) {
            case 1: {
                return new ChocolateSantaClaus(weight);
            }
            case 2: {
                return new CaramelCandy(weight);
            }
            case 3: {
                return new JellyCandy(weight);
            }
            case 4: {
                return new ChocolateCandy(weight);
            }default:{
                throw new IllegalArgumentException("You entered wrong number of sweets kind");
            }
        }
    }

    private boolean toContinue(Scanner scanner, String methodMarker) throws NumberLessThenZeroException {
        boolean continueWorking = false;
        System.out.println(DO_YOU_WANT_TO_CONTINUE_FIRST_PART + methodMarker +
                DO_YOU_WANT_TO_CONTINUE_SECOND_PART);
        int decision = scanner.nextInt();
        if (decision < 0) {

            throw new NumberLessThenZeroException("You have entered too low a value");
        }
        if (decision == 1) {
            continueWorking = true;
        }
        return continueWorking;
    }

}
