package com.mycompany.app.ooptask.menu;

public enum KindsOFSweets {
    SANTA("Chocolate Santa Claus"), CARAMEL("Caramel candy"), JELLY("Jelly candy"),
    CHOCOLATE("Chocolate candy");

    public String name;

    KindsOFSweets(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
